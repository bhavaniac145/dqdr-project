export interface ReconRules {
    "rule_id": string,
    "rule_name": string,
    "Rule_Description": string,
    "CreatedBy": string,
    "UpdatedBy": string,
    "CreatedDT": string,
    "UpdatedDT": string
}
