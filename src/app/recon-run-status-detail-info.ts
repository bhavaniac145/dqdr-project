export interface ReconRunStatusDetailInfo {
    "CRUN_ID": number,
    "PRUN_ID": number,
    "appl_code": string,
    "rule_id": string,
    "ST_ID": number,
    "CRUN_STATUS": string,
    "CRUN_STRT_TM": string,
    "CRUN_END_TM": string,
    "SRC_CNT": number,
    "TGT_CNT": number,
    "COLUMN_NM": string,
    "REASON": string
}
