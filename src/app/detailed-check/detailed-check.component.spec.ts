import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedCheckComponent } from './detailed-check.component';

describe('DetailedCheckComponent', () => {
  let component: DetailedCheckComponent;
  let fixture: ComponentFixture<DetailedCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
