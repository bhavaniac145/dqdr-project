import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DqdrService } from 'src/app/dqdr.service';
import { concat } from 'rxjs/internal/observable/concat';
import { ColumnData } from 'src/app/column-data';

@Component({
  selector: 'app-detailed-check',
  templateUrl: './detailed-check.component.html',
  styleUrls: ['./detailed-check.component.css']
})
export class DetailedCheckComponent implements OnInit {
  private param = {
    "appName": "",
    "appCode": "",
    "prunId": 0,
  }
  private objectName: string;
private appName: string;
private appCode: string;
private prunId: number;
private reconRunStatus: any = [];
private reconRules: any = [];
private reconrunStatusDeatilInfo:any = [];
private sourceTargetDetail:any = [];
private success = false;
private failure = false;
private pending = false;
private columnData: ColumnData = {};
private columnDataArray:ColumnData[] = [];
private applicationInfo:any = [];
private headers = ['Run Id', 'Table name', 'Column Name', 'Rule name', 'Job status','Job start time','Job end time','Source count','Target count','Reason']
// private objectName: string;
  constructor(private router: ActivatedRoute, private dqdrservice: DqdrService, private route: ActivatedRoute) { 
   
  }

  ngOnInit() {
    (this.route.queryParams.subscribe(params =>{
      console.log(params);
      this.appCode = params.appCode;
      this.appName = params.appName;
      this.objectName = params.objectName;
    }));
    console.log("appName " + this.appName);
    console.log("appCode " + this.appCode);
    this.dqdrservice.reconRunStatus().subscribe(data=>{
      this.reconRunStatus = data;
      // console.log(this.reconRunStatus);
      this.reconRunStatus.forEach(element => {
        if(element.appl_code===this.appCode){
          if(element.PRUN_STATUS==='S'){
            this.success = true;
          }
          else if(element.PRUN_STATUS==='F'){
            this.failure = true;
          }
          else if(element.PRUN_STATUS==='P'){
            this.pending = true;
          }
        }        
      });
    })

    // this.dqdrservice.reconRules().subscribe(data=>{
    //   this.reconRules = data;
    // });

    // this.dqdrservice.reconRunstatusDetailInfo().subscribe(data=>{
    //   this.reconrunStatusDeatilInfo = data;
    //   console.log(this.reconrunStatusDeatilInfo);
    // });

    // this.dqdrservice.sourceTargetDetail().subscribe(data=>{
    //   this.sourceTargetDetail = data;
    // });

    this.dqdrservice.applicationInfo().subscribe(applicationinfoEle=>{
      this.dqdrservice.reconRunStatus().subscribe(reconRunStatusEle=>{  
        this.applicationInfo = applicationinfoEle;
        console.log(this.applicationInfo);
        this.reconRunStatus = reconRunStatusEle;
        this.applicationInfo.forEach(applicationinfoVar => {
          this.reconRunStatus.forEach(reconRunStatusVar => {
            if(reconRunStatusVar.appl_code === applicationinfoVar.appl_code && applicationinfoVar.appl_name === this.appName){
              this.prunId = reconRunStatusVar.PRUN_ID;
              console.log("*********"+this.prunId);
          }
          });
        });
 
})
})

    this.dqdrservice.reconRules().subscribe(reconRulesVar => {      
      // console.log(reconRulesVar);
      this.reconRules = reconRulesVar;
      this.dqdrservice.reconRunstatusDetailInfo().subscribe(reconrunStatusDeatilInfoVar => {
        this.reconrunStatusDeatilInfo = reconrunStatusDeatilInfoVar;
        // console.log(reconrunStatusDeatilInfoVar);
        this.dqdrservice.sourceTargetDetail().subscribe(sourceTargetDetaiVar => {
          // console.log(sourceTargetDetaiVar);
          this.sourceTargetDetail = sourceTargetDetaiVar;
          this.reconRules.forEach(reconRulesVar => {
            // console.log(reconRulesVar);
              this.reconrunStatusDeatilInfo.forEach(reconrunStatusDeatilInfoVar => {
                // console.log(reconrunStatusDeatilInfoVar);
                this.sourceTargetDetail.forEach(sourceTargetDetaiVar => {
                  this.columnData = {};
                  // console.log(sourceTargetDetaiVar);
                  if(reconRulesVar.rule_id === reconrunStatusDeatilInfoVar.rule_id && 
                    reconrunStatusDeatilInfoVar.ST_ID === sourceTargetDetaiVar.ST_ID &&
                    sourceTargetDetaiVar.TGT_TBL_NM === this.objectName){ 
                      this.columnData.runId = reconrunStatusDeatilInfoVar.CRUN_ID;
                      this.columnData.tableName = sourceTargetDetaiVar.TGT_TBL_NM;
                      this.columnData.columnName = reconrunStatusDeatilInfoVar.COLUMN_NM;
                      this.columnData.ruleName = reconRulesVar.rule_name;
                      this.columnData.jobStatus = reconrunStatusDeatilInfoVar.CRUN_STATUS;
                      this.columnData.jobStartTime = reconrunStatusDeatilInfoVar.CRUN_STRT_TM;
                      this.columnData.jobEndTime = reconrunStatusDeatilInfoVar.CRUN_END_TM;
                      this.columnData.sourceCount = reconrunStatusDeatilInfoVar.SRC_CNT;
                      this.columnData.targetCount = reconrunStatusDeatilInfoVar.TGT_CNT;
                      this.columnData.reason = reconrunStatusDeatilInfoVar.REASON;            
                      this.columnDataArray.push(this.columnData);
                      // this.columnDataArray.push(this.columnData);
                      // this.columnDataArray.push(this.columnData);
                  }
                  
                });
              });
            });
              
         
            console.log(this.columnDataArray);
          
        });
       
        
      });
      
    });

  }

}
