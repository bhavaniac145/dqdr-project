import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisplayContentComponent } from 'src/app/display-content/display-content.component';
import { UserLoginComponent } from 'src/app/user-login/user-login.component';
import { DetailedCheckComponent } from 'src/app/detailed-check/detailed-check.component';


const routes: Routes = [
  {path: 'displayContent', component: DisplayContentComponent  },
  {path: 'userLogin', component: UserLoginComponent},
  {path: 'displayContent/:list', component: DetailedCheckComponent},
  {path:'', redirectTo:'/userLogin',pathMatch:'full'},
  {path:'detailCheck',component:DetailedCheckComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
