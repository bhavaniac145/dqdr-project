export interface ColumnData { 
    "runId"?: number,
   "tableName"?:  string,
   "columnName"?:  string,
   "ruleName"?:  string ,
   "jobStatus"?:  string,
   "jobStartTime"?:  string,
   "jobEndTime"?:  string,
   "sourceCount"?: number,
   "targetCount"?: number,
   "reason"?:  string
}
