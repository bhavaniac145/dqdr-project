import { Injectable } from '@angular/core';
import { ApplicationInformation } from 'src/app/application-information';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { SourceeTargetDetails } from 'src/app/sourcee-target-details';
import { ReconRunStatusDetailInfo } from 'src/app/recon-run-status-detail-info';
import { ReconRules } from 'src/app/recon-rules';
import { ReconRunStatus } from 'src/app/recon-run-status';

@Injectable({
  providedIn: 'root'
})
export class DqdrService {
private url = "http://localhost:3000/application_information";
private url1 = "http://localhost:3000/source_target_details";
private url2 = "http://localhost:3000/recon_run_status_detail_info";
private url3 = "http://localhost:3000/recon_rules";
private url4 = "http://localhost:3000/recon_run_status";

applicationInfo(): Observable<ApplicationInformation>{
  return this.http.get<ApplicationInformation>(this.url);
}

sourceTargetDetail(): Observable<SourceeTargetDetails>{
  return this.http.get<SourceeTargetDetails>(this.url1);
}

reconRunstatusDetailInfo():Observable<ReconRunStatusDetailInfo>{
  return this.http.get<ReconRunStatusDetailInfo>(this.url2);
}

reconRules():Observable<ReconRules>{
  return this.http.get<ReconRules>(this.url3);
}

reconRunStatus(): Observable<ReconRunStatus>{
  return this.http.get<ReconRunStatus>(this.url4);
}
  constructor(private http: HttpClient) { }
}
