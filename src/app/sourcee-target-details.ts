
export interface SourceeTargetDetails {
    "ST_ID": number,
    "appl_code": string,
    "SRC_TYP": string,
    "SRC_TBL_NM": string,
    "SRC_TBL_COLUMN_NM":string,
    "TGT_TYP": string,
    "TGT_TBL_NM": string,
    "TGT_TBL_COLUMN_NM":string,
    "CreatedBy": string ,
    "UpdatedBy": string
}
