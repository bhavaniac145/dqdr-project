export interface ReconRunStatus {
    "PRUN_ID": number,
    "appl_code": string,
    "PRUN_START_Time": string,
    "PRUN_END_Time": number,
    "PRUN_STATUS":string,
    "Run_By": string
}
