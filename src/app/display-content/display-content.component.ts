import { Component, OnInit } from '@angular/core';
import { DqdrService } from 'src/app/dqdr.service';
import { ApplicationInformation } from 'src/app/application-information';
import { SourceeTargetDetails } from 'src/app/sourcee-target-details';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-display-content',
  templateUrl: './display-content.component.html',
  styleUrls: ['./display-content.component.css']
})
export class DisplayContentComponent implements OnInit {

  private applicationInfo:any = [];
  private reconRunStatus:any = [];
  private applicationCode:ApplicationInformation;
  private var: string;
  private appName: string;
  private appCode: string;
  private objectName: string;
  private prunId:number;
  private param = {
    "appName": "",
    "appCode": "",
    "prunId": 0,
  }
  private sourceTargetInfo: SourceeTargetDetails[] = [];
  private newSourceTargetInfo:any = [];  
  private show: boolean = false;

  constructor(private dqdrservice:DqdrService, private router:Router) {
   }

  ngOnInit() {

    this.dqdrservice.applicationInfo().subscribe(data=>{
      this.applicationInfo = data;
      console.log(this.applicationInfo);
    });   

  }

 filterData() {
    this.show = true;
    console.log(this.applicationCode);
    this.appName = this.applicationCode.appl_name;
    this.appCode = this.applicationCode.appl_code;
    
    this.dqdrservice.sourceTargetDetail().subscribe(data=>{ 
      this.newSourceTargetInfo = data;
      console.log(this.newSourceTargetInfo);
      this.newSourceTargetInfo.forEach(element => {
        if(this.appCode === element.appl_code){
          this.sourceTargetInfo.push(element);
        }
      }); 
  });
  this.sourceTargetInfo = [];
  } 

  displayInfo(){
    const navigationExtras: NavigationExtras = {
      queryParams: {
        appName: this.appName,
        appCode: this.appCode,
        objectName: this.objectName
      }
    }
    console.log(navigationExtras);
    this.router.navigate(['detailCheck'],navigationExtras);
  }

  }


