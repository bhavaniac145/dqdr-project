import { TestBed } from '@angular/core/testing';

import { DqdrService } from './dqdr.service';

describe('DqdrService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DqdrService = TestBed.get(DqdrService);
    expect(service).toBeTruthy();
  });
});
