import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } 
    from '@angular/forms';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  
  private show: boolean = false;
  form: FormGroup;
  logInEmail = new FormControl("", Validators.required);
  logInPassword = new FormControl("", Validators.required);
  constructor(private router: Router, private fb: FormBuilder) {
    this.form = fb.group({
      logInEmail: [''],
      logInPassword:[''],
  });
   }


  ngOnInit() {
  }

  back(event) {
    var signUp = document.getElementById("signUp");
    var signIn = document.getElementById("signIn");
    
    signIn.classList.add("active-sx"); 
    signUp.classList.add("inactive-dx");
    signUp.classList.remove("active-dx");
    signIn.classList.remove("inactive-sx");
  }

  logIn(event){
    var signUp = document.getElementById("signUp");
    var signIn = document.getElementById("signIn");

  signUp.classList.add("active-dx");
  signIn.classList.add("inactive-sx");
  signIn.classList.remove("active-sx");
  signUp.classList.remove("inactive-dx");
  }

  SignIn(data){
    console.log(data.logInEmail + " " + data.logInPassword);
    if(data.logInEmail==="admin" && data.logInPassword==="admin"){
      this.router.navigate(['displayContent']);
      console.log("router done");
    }
    else{
      console.log("show");
      this.show = true;
    }
  }

}
